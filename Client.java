package entity;
import java.io.IOException;
import java.util.ArrayList;

import konstante.*;
import manage.BookedManager;
import manage.ClientManager;

public class Client extends Person{
    private double spentMoney = 0;
    private boolean loyaltyCard = false;

    public Client(int id, String firstName, String lastName, String gender, String phoneNumber, String adress, String userName, String password, double spentMoney,boolean loyaltyCard,boolean deleted) {
        super(id, firstName, lastName, gender, phoneNumber, adress, userName, password,deleted);
        this.spentMoney = spentMoney;
        this.loyaltyCard = loyaltyCard;
    }
    public double getSpentMoney() {
        return spentMoney;
    }
    public void setSpentMoney(double spentMoney) {
        this.spentMoney = spentMoney;
    }
    public boolean getLoyaltyCard() {
        return loyaltyCard;
    }
    public void setLoyaltyCard(boolean loyaltyCard) {
        this.loyaltyCard = loyaltyCard;
    }

    //----------------------------------------------------------------------------------
    // klijent zakazuje
    //----------------------------------------------------------------------------------
    public static Beautician pickBeauticianAuto(int id, ArrayList<Beautician> allBeauticians) {
    	int numberOfServices = (int) Double.POSITIVE_INFINITY;
    	Beautician picked = null;
    	for(Beautician beautician : allBeauticians) {
    		if(beautician.getTreatments().contains(id)){
    			if(beautician.getNumberOfGivenTreatments() < numberOfServices ) {
    				numberOfServices = beautician.getNumberOfGivenTreatments();
    				picked = beautician;
    			}
    		}
    	}
    	return picked;
    }
    public static ArrayList<Beautician> getPossibleBeauticians(int id, ArrayList<Beautician> allBeauticians){
    	ArrayList<Beautician> beauticians = new ArrayList<>();
    	for(Beautician beautician : allBeauticians) {
    		if(beautician.getTreatments().contains(id)){
    			beauticians.add(beautician);
    		}
    	}
    	return beauticians;
    }
    //------------------------------------------------------------------
    // pretraga za zakazivanje, tip trajane cena
    //------------------------------------------------------------------
    public static ArrayList<ServiceTreatment> search(String typeOfTreatment, int maxPrice, int maxDuration, ArrayList<ServiceTreatment> allServiceTreatments) {
    	ArrayList<ServiceTreatment> services = new ArrayList<>();
    	for(ServiceTreatment serviceTreatment : allServiceTreatments) {
    		if((serviceTreatment.getNameOfService().equals(typeOfTreatment) || serviceTreatment.getNameOfService().equals(""))  && (serviceTreatment.getTime() <= maxDuration || serviceTreatment.getTime() == -1) && (serviceTreatment.getPrice()<=maxPrice || serviceTreatment.getPrice()<=maxPrice)) {
    			services.add(serviceTreatment);
    		}
    	}
    	return services;
    }
}























/*
    
    public static boolean hasLoyaltyCardAcess(ArrayList<Person> people, String userNameOfClient){
        double minimum = Manager.getMinumumForLoyaltyCard();
        for(Person person : people){
            if(person.getUserName().equals(userNameOfClient)){
                Client samePerson = (Client) person;
                if(samePerson.spentMoney>minimum){
                    return true;
                }
            }
        }
        return false;
}
    */