package entity;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import konstante.*;
import manage.BeauticianManager;
import manage.BookedManager;
import manage.ClientManager;
import manage.ManagerManager;

public class Manager extends Employe{

    public Manager(int id, String firstName, String lastName, String gender, String phoneNumber, String adress,String userName, String password, education levelOfEducation, double experience, double bonus, double paycheck,boolean deleted) {
        super(id, firstName, lastName, gender, phoneNumber, adress, userName, password, levelOfEducation, experience, bonus, paycheck,deleted);
    }










    // SET PAYCHECK
    public static double setPaycheck(education leverOfEducation, double experience){
        double paycheck = 40000; //osnova
        double experienceBonus = experience * 0.05;
        double educationBonus;
        switch(leverOfEducation) {
            case HIGH_SCHOOL:
                educationBonus = 0.05;
                break;
            case UNIVERSITY:
                educationBonus = 0.10;
                break;
            case MASTERS:
                educationBonus = 0.15;
                break;
            case DOCTORATE:
                educationBonus = 0.20;
                break;
            default:
                educationBonus = 0;
        }
        return paycheck * (1 + experienceBonus + educationBonus);
    }
    // 10% OFF
    public static double tenPercentOff(ServiceTreatment type, Client client){
        if(client.getLoyaltyCard()){
            return 0.9*type.getPrice();
        }
        return type.getPrice();
    }
    // GIVE LOYALTY CARD
    public static void getLoyaltyCard(ClientManager clientManager, Client client) throws IOException{
        ArrayList<Client> clients = clientManager.read();
        if(client.getSpentMoney() > getMinumumForLoyaltyCard()){
            client.setLoyaltyCard(true);
            clients.remove(client);
            clientManager.write(clients);
        }
    }
    // SET MINIMUM FOR CARD
    public static double getMinumumForLoyaltyCard(){
        return 5000;
    }
}