package manage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import entity.Beautician;
import entity.Booked;
import entity.Global;
import entity.Manager;
import entity.Receptionist;
import entity.Treatment;
import konstante.education;
import konstante.status;

public class ManagerManager {
    private static String managerFile;
    private static ArrayList<Manager> managers;

    public ManagerManager(String managerFile){
        this.managers = new ArrayList<Manager>();
        this.managerFile = managerFile;
    }

    // read and write
    public ArrayList<Manager> read() throws NumberFormatException, IOException{
        ArrayList<Manager> managers = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(managerFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                if(values[0].equals(""))
                	break;
                Manager manager = new Manager(Integer.parseInt(values[0]), values[1], values[2], values[3], values[4], values[5], values[6], values[7], Enum.valueOf(education.class, values[8]), Double.parseDouble(values[9]), Double.parseDouble(values[10]), Double.parseDouble(values[11]), Boolean.valueOf(values[12]));
                managers.add(manager);
                }

            }
        return managers;
    }

    public void write(ArrayList<Manager> managers) throws IOException{
        try (FileWriter writer = new FileWriter(managerFile, false)) {
            StringBuilder stringBuilder = new StringBuilder();
            for(Manager person : managers){
            stringBuilder.append(person.getId() + ",");
            stringBuilder.append(person.getFirstName() + ",");
            stringBuilder.append(person.getLastName() + ",");
            stringBuilder.append(person.getGender() + ",");
            stringBuilder.append(person.getPhoneNumber() + ",");
            stringBuilder.append(person.getAdress() + ",");
            stringBuilder.append(person.getUserName() + ",");
            stringBuilder.append(person.getPassword() + ",");
            stringBuilder.append(person.getLevelOfEducation() + ",");
            stringBuilder.append(person.getExperience() + ",");
            stringBuilder.append(person.getBonus() + ",");
            stringBuilder.append(person.getPaycheck() + ",");
            stringBuilder.append(person.getDeleted());
            stringBuilder.append("\n");
            writer.append(stringBuilder.toString());
            stringBuilder.setLength(0);
            }
            stringBuilder.setLength(0);
            writer.flush();
        }
    }

    // create
    public Manager registerWorkers(ArrayList<Manager> managers,String firstName, String lastName, String gender, String phoneNumber, String adress, String userName, String password, education levelOfEducation, double experience, double bonus) throws IOException{
        double paycheck = Manager.setPaycheck(levelOfEducation, experience);
        Manager menager = new Manager(Global.userID, firstName, lastName, gender, phoneNumber, adress, userName, password, levelOfEducation, experience, bonus, paycheck,false);
        managers.add(menager);
        write(managers);
        Global.userID++;
        return menager;
    }
    // update
    public boolean editManager(ArrayList<Manager> managers, String oldUserName, String firstName, String lastName, String gender, String phoneNumber, String adress, String newUserName, String password, education levelOfEducation, double experience, double bonus, double paycheck) throws IOException{
        for(Manager person : managers){
            if(person.getUserName().equals(newUserName)){
                return false; //user name taken
            }
        }
        for(Manager person : managers){
            if(person.getUserName().equals(oldUserName)){
                deleteManager(managers, person.getId());
                person.setFirstName(firstName);
                person.setLastName(lastName);
                person.setGender(gender);
                person.setAdress(adress);
                person.setGender(gender);
                person.setUserName(newUserName);
                person.setPassword(password);
                person.setLevelOfEducation(levelOfEducation);
                person.setBonus(bonus);
                person.setExperience(experience);
                person.setPaycheck(paycheck);
                Manager manager = new Manager(person.getId(),firstName,lastName,gender,phoneNumber,adress, newUserName,password,levelOfEducation,experience, bonus, paycheck,person.getDeleted());
                return true;
            }
        }
        return false;
    }
    //delete
    public void deleteManager(ArrayList<Manager> managers, int id) throws IOException{
        for(Manager manager : managers) {
        	if(manager.getId() == id) {
        		managers.remove(manager);
        		this.write(managers);
        	}
        }
    }
    /*
     * za dijagram
     * vraca kozmeticare sa kolicinom tretmana koje su izvrsili
     * samo COMPLETED
     */
    public HashMap<String, ArrayList<Integer>> getBeauticians(ArrayList<Booked> allBooked, SimpleDateFormat dateFormat, LocalDate startDate, LocalDate endDate) {
    	HashMap<String, ArrayList<Integer>> mapa = new HashMap<>();
        for(Booked booked : allBooked) {
        	if(booked.getStatusOfTreatment().equals(status.COMPLETED)) {
        		LocalDate bookedDate = LocalDate.parse(booked.getDate(), DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        		if(bookedDate.isBefore(endDate)&&bookedDate.isAfter(startDate)) {
        			if(mapa.containsKey(booked.getUserNameOfBeautician())) {
        			    ArrayList<Integer> worth = mapa.get(booked.getUserNameOfBeautician());
        				worth.set(0,(int) (worth.get(0)+booked.getPrice()));
        				worth.set(1, worth.get(1)+1);
        				mapa.put(booked.getUserNameOfBeautician(), worth);
        			}else {
        				ArrayList<Integer> worth = new ArrayList<>(Arrays.asList(0, 0));
        				worth.set(0,(int) booked.getPrice());
        				worth.set(1, 1);
        				mapa.put(booked.getUserNameOfBeautician(), worth);
        			}
        		}
        	}
        }
		return mapa;
    }
    /* 
     * za dijagram
     * prikaz tretmana po statusu za odredjeni period
     * hes mapa gde su kljucevi statusi a vrednosti koliko tretmana nosi te 
     * statuse u odredjenom periodu
     * nepotrebna validacija za kraj pre pocetka samo ce vratiti prazno
     */
    public HashMap<status, Integer> getBookedBasedOnStatus(ArrayList<Booked> allBooked,LocalDate startDate,LocalDate endDate,SimpleDateFormat dateFormat) {
	    HashMap<status, Integer> mapa2 = new HashMap<>();
	    mapa2.put(status.COMPLETED, 0);
	    mapa2.put(status.CANCELED_BY_THE_SALON, 0);
	    mapa2.put(status.CANCELED_BY_CLIENT, 0);
	    mapa2.put(status.BOOKED, 0);
	    mapa2.put(status.NO_SHOW, 0);
	    for(Booked b : allBooked) {
    		LocalDate bookedDate = LocalDate.parse(b.getDate(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    		if(bookedDate.isBefore(endDate)&&bookedDate.isAfter(startDate)) {
    			switch (b.getStatusOfTreatment()) {
    		    case COMPLETED:
    		    	mapa2.put(status.COMPLETED, mapa2.get(status.COMPLETED)+1);
    		        break;
    		    case BOOKED:
    		    	mapa2.put(status.BOOKED, mapa2.get(status.BOOKED)+1);
    		        break;
    		    case CANCELED_BY_CLIENT:
    		    	mapa2.put(status.COMPLETED, mapa2.get(status.COMPLETED)+1);
    		        break;
    		    case CANCELED_BY_THE_SALON:
    		    	mapa2.put(status.CANCELED_BY_THE_SALON, mapa2.get(status.CANCELED_BY_THE_SALON)+1);
    		    case NO_SHOW:
    		    	mapa2.put(status.NO_SHOW, mapa2.get(status.NO_SHOW)+1);
    		    	break;
				default:
					break;
	    		}

		    }
	    }
		return mapa2;
    }
    /*
     * ovo su samo prihodi po mesecima - za dijagram
     * izvestaj je na godisnjem nivou pa hes mapa ima 12 kljuceva
     * konstantno kreiranje novih hes mapa da bi se izbeglo kopiranje
     * vrednosti za svaki mesec je nova hes mapa koja ima imena tretmana kao kljuceve
     * a prinose na osnovu statusa za vrednosti
     */
    public HashMap<Double, HashMap<String, Double>> prihodi(ArrayList<Booked> allBooked, ArrayList<Treatment> allTreatments) {
    	double add = 0;
    	LocalDate today = LocalDate.now();
    	LocalDate dateMinus12Months = today.minusYears(1);
    	HashMap<String, Double> typeMoney = new HashMap<>();
    	HashMap<Double, HashMap<String, Double>> mapa = new HashMap<>();
    	HashMap<String, Double> typeMoneyCopy = new HashMap<>();
    	for(Treatment t : allTreatments) {
    		typeMoney.put(t.getNameOfService(), (double) 0);
    	}

    	mapa.put(1.0, new HashMap<>(typeMoney));
    	mapa.put(2.0, new HashMap<>(typeMoney));
    	mapa.put(3.0, new HashMap<>(typeMoney));
    	mapa.put(4.0, new HashMap<>(typeMoney));
    	mapa.put(5.0, new HashMap<>(typeMoney));
    	mapa.put(6.0, new HashMap<>(typeMoney));
    	mapa.put(7.0, new HashMap<>(typeMoney));
    	mapa.put(8.0, new HashMap<>(typeMoney));
    	mapa.put(9.0, new HashMap<>(typeMoney));
    	mapa.put(10.0, new HashMap<>(typeMoney));
    	mapa.put(11.0, new HashMap<>(typeMoney));
    	mapa.put(12.0, new HashMap<>(typeMoney));

    	for (Booked booked : allBooked) {
    	    LocalDate bookedDate = LocalDate.parse(booked.getDate(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    	    if (bookedDate.isAfter(dateMinus12Months) && bookedDate.isBefore(today)) {
    	        double keyy = bookedDate.getMonthValue();
    	        switch (booked.getStatusOfTreatment()) {
    	            case CANCELED_BY_THE_SALON:
    	                add = 0;
    	                break;
    	            case CANCELED_BY_CLIENT:
    	                add = 0.1 * booked.getPrice();
    	                break;
    	            default:
    	                add = booked.getPrice();
    	                break;
    	        }
    	        
    	        String typeOfTreatment = booked.getTypeOfTreatment();
    	        typeMoney = mapa.get(keyy);
    	        double worth = typeMoney.getOrDefault(typeOfTreatment, 0.0);
    	        worth += add;
    	        typeMoney.put(typeOfTreatment, worth);
    	        
    	        mapa.put(keyy, new HashMap<>(typeMoney));
    	        
    	    }
    	}

		return mapa;
    }
    // pomocna za linearni dijagram
    public static double[] shiftList(List<Integer> lst, int n) {
        int index = lst.indexOf(n);
        double[] shiftedArray = new double[lst.size()];

        for (int i = 0; i < lst.size(); i++) {
            shiftedArray[i] = lst.get((index + i) % lst.size());
        }

        return shiftedArray;
    }
    /*
     * pravim hes mapu koja kao kljuceve ima korisnicka imena radnika
     * vrednosti su liste gde je prva vrednost prihod a druga rashod
     * recepcioner nema prihode samo rashode u vidu plate i bonusa
     * dodajem kljuc ukupno i tu sabiram sve kako bi se prikazalo na dnu tabele
     */
    public HashMap<String, ArrayList<Double>> bilans(ArrayList<Receptionist> allReceptionists,ArrayList<Booked> allBooked, ArrayList<Beautician> allBeauticians, LocalDate start, LocalDate end) {
        HashMap<String, ArrayList<Double>> mapa = new HashMap<>();
        String mainKey = "UKUPNO";
        ArrayList<Double> mainValues = new ArrayList<>();
        mainValues.add(0.0);
        mainValues.add(0.0);
        mapa.put(mainKey, mainValues);
        
        int n = countFirstDaysInMonth(start, end);
        
        for (Beautician beautician : allBeauticians) {
            String key = beautician.getUserName();
            ArrayList<Double> values = new ArrayList<>();
            values.add(0.0);
            values.add(0.0);
            for (Booked booked : allBooked) {
                LocalDate bookedDate = LocalDate.parse(booked.getDate(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                if (bookedDate.isEqual(start) || bookedDate.isEqual(end) || (bookedDate.isAfter(start) && bookedDate.isBefore(end))) {
                    if(!booked.getUserNameOfClient().equals("") && booked.getUserNameOfBeautician().equals(beautician.getUserName())) {
	                	double add = booked.getPrice();
	                    switch (booked.getStatusOfTreatment()) {
	                        case CANCELED_BY_THE_SALON:
	                            add = 0;
	                            break;
	                        case CANCELED_BY_CLIENT:
	                            add = 0.1 * booked.getPrice();
	                            break;
	                        default:
	                            add = booked.getPrice();
	                            break;
	                    }
	                    values.set(0, values.get(0) + add);
	                }
                }
            }
            if(n != 0) {
            	for(int i = 0; i<n; i++) {
            		values.set(1, values.get(1) + beautician.getPaycheck() + beautician.getBonus());
            	}
            }
            mapa.put(key, values);
            mainValues.set(0, mainValues.get(0) + values.get(0));
            mainValues.set(1, mainValues.get(1) + values.get(1));
            mapa.put(mainKey, mainValues);
        }
        for(Receptionist receptionist : allReceptionists) {
        	String key = receptionist.getUserName();
            ArrayList<Double> values = new ArrayList<>();
            values.add(0.0);
            values.add(0.0);
        	for(int i = 0; i<n; i++) {
        		values.set(1, values.get(1) + receptionist.getPaycheck());
        	}
            mapa.put(key, values);
            mainValues.set(0, mainValues.get(0) + values.get(0));
            mainValues.set(1, mainValues.get(1) + values.get(1));
            mapa.put(mainKey, mainValues);
        }
        
        return mapa;
    }
    /*
     * pomocna funckija za odredjivanje koliko puta se treba dodati plata kao rashod
     * izmedju dva datuma odrediti koliko prvih u mesecu se javlja
     */
    public static int countFirstDaysInMonth(LocalDate startDate, LocalDate endDate) {
        int count = 0;

        LocalDate currentMonthStart = startDate.withDayOfMonth(1);
        YearMonth currentYearMonth = YearMonth.from(currentMonthStart);

        if (currentMonthStart.isBefore(startDate)) {
            currentMonthStart = currentYearMonth.plusMonths(1).atDay(1);
            currentYearMonth = YearMonth.from(currentMonthStart);
        }

        while (!currentMonthStart.isAfter(endDate)) {
            count++;
            currentMonthStart = currentYearMonth.plusMonths(1).atDay(1);
            currentYearMonth = YearMonth.from(currentMonthStart);
        }

        return count;
    }



}
