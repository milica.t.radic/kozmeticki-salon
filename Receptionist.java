package entity;

import java.io.IOException;
import java.util.ArrayList;

import konstante.education;
import konstante.status;
import manage.BookedManager;
import manage.ClientManager;

public class Receptionist extends Employe{
    public Receptionist(int id, String firstName, String lastName, String gender, String phoneNumber, String adress,String userName, String password, education levelOfEducation, double experience, double bonus, double paycheck,boolean deleted) {
        super(id, firstName, lastName, gender, phoneNumber, adress, userName, password, levelOfEducation, experience, bonus, paycheck,deleted);
    }
    public static void cancelBooked(ClientManager cm, BookedManager bm, Booked booked, ArrayList<Booked> allBooked, Client client, ArrayList<Client> allClients) throws IOException {
    	allBooked.remove(booked);
    	allClients.remove(client);
    	booked.setStatusOfTreatment(status.CANCELED_BY_THE_SALON);
    	int money = (int) (client.getSpentMoney() - booked.getPrice());
    	client.setSpentMoney(money);
    	allBooked.add(booked);
    	allClients.add(client);
    	bm.write(allBooked);
    	cm.write(allClients);
    }
}


