package manage;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import entity.Global;
import entity.Treatment;
import entity.ServiceTreatment;

public class ServiceTreatmentManager {
    private static String serviceTreatmentFile;
    private ArrayList<ServiceTreatment> types;

    public ServiceTreatmentManager(String serviceTreatmentFile){
        this.types = new ArrayList<ServiceTreatment>();
        this.serviceTreatmentFile = serviceTreatmentFile;
    }

    // read and write
    public ArrayList<ServiceTreatment> read() {
        ArrayList<ServiceTreatment> allTypeOfTreatments = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(serviceTreatmentFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                if(values[0].equals(""))
                	break;
                ServiceTreatment treatment = new ServiceTreatment(Integer.parseInt(values[0]), values[1],values[2] ,Integer.parseInt(values[3]), Double.parseDouble(values[4]));
                allTypeOfTreatments.add(treatment);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allTypeOfTreatments;
    }
    public static void write(ArrayList<ServiceTreatment> serviceTreatments) throws IOException{
        try (FileWriter writer = new FileWriter(serviceTreatmentFile, false)) {
            for(ServiceTreatment serviceTreatment : serviceTreatments){
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(String.valueOf(serviceTreatment.getId()) + ",");
            stringBuilder.append(serviceTreatment.getNameOfService() + ",");
            stringBuilder.append(serviceTreatment.getTypeOfTheTreatment() + ",");
            stringBuilder.append(serviceTreatment.getTime() + ",");
            stringBuilder.append(serviceTreatment.getPrice());
            stringBuilder.append("\n");
            writer.append(stringBuilder.toString());
            stringBuilder.setLength(0);
            }
            writer.flush();
        }
    }
    // CREATE TYPE OF TREATMENT
    public ServiceTreatment createServiceTreatments(String nameOfService, String typeOfTheTreatment, int time, double price) throws IOException{
    	ArrayList<ServiceTreatment> types = read();
    	ServiceTreatment type = new ServiceTreatment(Global.treatmentID, nameOfService, typeOfTheTreatment,time, price);
        Global.treatmentID ++;
        types.add(type);
        write(types);
        return type;
    }
    // DELETE ONE TYPE OF THE TREATMENT you can no longer get this service
    public boolean deleteServiceTreatment(TreatmentManager treatmentManager,ArrayList<ServiceTreatment> allServiceTreatments,ArrayList<Treatment> allTreatments, int id) throws IOException{
    	ServiceTreatment type = null;
    	for(ServiceTreatment serviceTreatment : allServiceTreatments) {
    		if(serviceTreatment.getId() == id) {
    			type = serviceTreatment;
    			allServiceTreatments.remove(serviceTreatment);
    			write(allServiceTreatments);
    			break;
    		}
    	}

        for(Treatment treatment : allTreatments){
            if(treatment.getNameOfService().equals(type.getNameOfService())){
                for(int oneTreatment : treatment.getListOfTypesOfTreatments()){
                    if(oneTreatment == type.getId()){
                        treatment.getListOfTypesOfTreatments().remove(Integer.valueOf(oneTreatment));
                        treatmentManager.updateTreatment(treatment,treatment.getListOfTypesOfTreatments());
                        return true;
                    }
                }
            }
        }
        return false;
    }
    // EDIT THE TREATMENT
    public ArrayList<ServiceTreatment> updateServiceTreatment(int id,ArrayList<ServiceTreatment> allServiceTreatments, String newType, int time, double newPrice) throws IOException{
    	for(ServiceTreatment serviceTreatment : allServiceTreatments) {
    		if(serviceTreatment.getId() == id) {
    			allServiceTreatments.remove(serviceTreatment);
    	    	serviceTreatment.setTypeOfTheTreatment(newType);
    	    	serviceTreatment.setTime(time);
    	    	serviceTreatment.setPrice(newPrice);
    	    	allServiceTreatments.add(serviceTreatment);
    	        write(allServiceTreatments);
    	        return allServiceTreatments;
    		}
    	}
		return allServiceTreatments;

    }
    public ServiceTreatment getByID(int id) {
    	ArrayList<ServiceTreatment> serviceTreatments = read();
    	for(ServiceTreatment serviceTreatment : serviceTreatments) {
    		if(serviceTreatment.getId() == id) {
    			return serviceTreatment;
    		}
    	}
    	return null;
    }
    public void deleteByID(int id) throws IOException {
    	ArrayList<ServiceTreatment> serviceTreatments = read();
    	for(ServiceTreatment serviceTreatment : serviceTreatments) {
    		if(serviceTreatment.getId() == id) {
    			int index = serviceTreatments.indexOf(serviceTreatment);
    			serviceTreatments.remove(index);
    			write(serviceTreatments);
    			break;
    		}
    	}
    }

}
