package entity;
import konstante.*;

public class Employe extends Person{
    private education levelOfEducation;
    private double experience;
    private double bonus;
    private double paycheck;

    Employe(int id, String firstName, String lastName, String gender, String phoneNumber,String adress,String userName, String password, education levelOfEducation, double experience, double bonus, double paycheck,boolean deleted) {
        super(id, firstName, lastName, gender, phoneNumber, adress, userName, password, deleted);
        this.levelOfEducation = levelOfEducation;
        this.experience = experience;
        this.bonus = bonus;
        this.paycheck = paycheck;
    }
    
    public education getLevelOfEducation() {
        return levelOfEducation;
    }
    public void setLevelOfEducation(education levelOfEducation) {
        this.levelOfEducation = levelOfEducation;
    }
    public double getExperience() {
        return experience;
    }
    public void setExperience(double experience) {
        this.experience = experience;
    }
    public double getBonus() {
        return bonus;
    }
    public void setBonus(double bonus) {
        this.bonus = bonus;
    }
    public double getPaycheck() {
        return paycheck;
    }
    public void setPaycheck(double paycheck) {
        this.paycheck = paycheck;
    }

}