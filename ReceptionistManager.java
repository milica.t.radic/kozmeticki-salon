package manage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import entity.Booked;
import entity.Client;
import entity.Global;
import entity.Manager;
import entity.Receptionist;
import konstante.education;
import konstante.status;

public class ReceptionistManager {
    private static String receptionistFile;
    private static ArrayList<Receptionist> receptionists;

    public ReceptionistManager(String receptionistFile){
        this.receptionists = new ArrayList<Receptionist>();
        this.receptionistFile = receptionistFile;
    }

    //read and write
    public ArrayList<Receptionist> read() throws NumberFormatException, IOException{
        ArrayList<Receptionist> receptionists = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(receptionistFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                if(values[0].equals(""))
                	break;
                Receptionist receptionist = new Receptionist(Integer.parseInt(values[0]), values[1], values[2], values[3], values[4], values[5], values[6], values[7], Enum.valueOf(education.class, values[8]), Double.parseDouble(values[9]), Double.parseDouble(values[10]), Double.parseDouble(values[11]),Boolean.valueOf(values[12]));
                receptionists.add(receptionist);
            }
        }
        return receptionists;
    }

    public void write(ArrayList<Receptionist> receptionists) throws IOException{
        try (FileWriter writer = new FileWriter(receptionistFile, false)) {
            StringBuilder stringBuilder = new StringBuilder();
            for(Receptionist person : receptionists){
            stringBuilder.append(person.getId() + ",");
            stringBuilder.append(person.getFirstName() + ",");
            stringBuilder.append(person.getLastName() + ",");
            stringBuilder.append(person.getGender() + ",");
            stringBuilder.append(person.getPhoneNumber() + ",");
            stringBuilder.append(person.getAdress() + ",");
            stringBuilder.append(person.getUserName() + ",");
            stringBuilder.append(person.getPassword() + ",");
            stringBuilder.append(person.getLevelOfEducation() + ",");
            stringBuilder.append(person.getExperience() + ",");
            stringBuilder.append(person.getBonus() + ",");
            stringBuilder.append(person.getPaycheck() + ",");
            stringBuilder.append(person.getDeleted());
            stringBuilder.append("\n");
            writer.append(stringBuilder.toString());
            stringBuilder.setLength(0);
            }
            stringBuilder.setLength(0);
            writer.flush();
        }
    }
    //create
    public Receptionist registerWorkers(String firstName, String lastName, String gender, String phoneNumber, String adress, String userName, String password, education levelOfEducation, double experience, double bonus) throws IOException{
        ArrayList<Receptionist> receptionists = read();
        double paycheck = Manager.setPaycheck(levelOfEducation, experience);
        Receptionist receptionist = new Receptionist(Global.userID, firstName, lastName, gender, phoneNumber, adress, userName, password, levelOfEducation, experience, bonus, paycheck,false);
        receptionists.add(receptionist);
        write(receptionists);
        Global.userID++;
        return receptionist;
    }
    //edit
    public boolean editReceptionist(ArrayList<Receptionist> receptionists, String oldUserName, String firstName, String lastName, String gender, String phoneNumber, String adress, String newUserName, String password, education levelOfEducation, double experience, double bonus, double paycheck) throws IOException{
        for(Receptionist person : receptionists){
            if(person.getUserName().equals(newUserName)){
                return false; //user name taken
            }
        }
        for(Receptionist person : receptionists){
            if(person.getUserName().equals(oldUserName)){
                deleteReceptionist(receptionists, person.getId());
                person.setFirstName(firstName);
                person.setLastName(lastName);
                person.setGender(gender);
                person.setAdress(adress);
                person.setGender(gender);
                person.setUserName(newUserName);
                person.setPassword(password);
                person.setLevelOfEducation(levelOfEducation);
                person.setBonus(bonus);
                person.setExperience(experience);
                person.setPaycheck(paycheck);
                Receptionist receptionist = new Receptionist(person.getId(), firstName,lastName,gender,phoneNumber,adress, newUserName,password,levelOfEducation,experience,bonus, paycheck,person.getDeleted());
                receptionists.add(receptionist);
                write(receptionists);
                return true;
                }
            }
        return false;
    }
    // delete
    public void deleteReceptionist(ArrayList<Receptionist> receptionists, int id) throws IOException{
        for(Receptionist receptionist : receptionists) {
        	if(receptionist.getId() == id) {
        		receptionists.remove(receptionist);
        		this.write(receptionists);
        		break;
        	}
        }
    }
    public ArrayList<Booked> cancelBooked(ClientManager cm, BookedManager bm, Booked booked, ArrayList<Booked> allBooked, Client client, ArrayList<Client> allClients) throws IOException {
    	allBooked.remove(booked);
    	allClients.remove(client);
    	booked.setStatusOfTreatment(status.CANCELED_BY_THE_SALON);
    	int money = (int) (client.getSpentMoney() - booked.getPrice());
    	client.setSpentMoney(money);
    	allBooked.add(booked);
    	allClients.add(client);
    	bm.write(allBooked);
    	cm.write(allClients);
    	return allBooked;
    }
}
