package manage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import entity.*;
import konstante.education;
import konstante.status;

public class BeauticianManager {
    private static String beauticianFile;
    private static ArrayList<Beautician> beauticians;

    public BeauticianManager(String beauticianFile){
        this.beauticians = new ArrayList<Beautician>();
        this.beauticianFile = beauticianFile;
    }
    //read and write
    public ArrayList<Beautician> read(){
        ArrayList<Beautician> allUsers = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(beauticianFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                if(values[0].equals(""))
                	break;
                String[] tl = values[12].split(";");
                ArrayList<Integer> treatmentsList = new ArrayList<Integer>();
                for(String t : tl) {
                	treatmentsList.add(Integer.parseInt(t));
                }
                Beautician beautician = new Beautician(Integer.parseInt(values[0]), values[1], values[2], values[3], values[4], values[5], values[6], values[7], Enum.valueOf(education.class, values[8]), Double.parseDouble(values[9]), Double.parseDouble(values[10]), Double.parseDouble(values[11]), treatmentsList, Integer.parseInt(values[13]), Boolean.valueOf(values[14]));
                allUsers.add(beautician);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allUsers;
    }
    public void write(ArrayList<Beautician> beauticians) throws IOException{
        try (FileWriter writer = new FileWriter(beauticianFile, false)) {
            StringBuilder stringBuilder = new StringBuilder();
            for(Beautician person : beauticians){
            stringBuilder.append(person.getId() + ",");
            stringBuilder.append(person.getFirstName() + ",");
            stringBuilder.append(person.getLastName() + ",");
            stringBuilder.append(person.getGender() + ",");
            stringBuilder.append(person.getPhoneNumber() + ",");
            stringBuilder.append(person.getAdress() + ",");
            stringBuilder.append(person.getUserName() + ",");
            stringBuilder.append(person.getPassword() + ",");
            stringBuilder.append(person.getLevelOfEducation() + ",");
            stringBuilder.append(person.getExperience() + ",");
            stringBuilder.append(person.getBonus() + ",");
            stringBuilder.append(person.getPaycheck() + ",");
            for(int i = 0 ; i < person.getTreatments().size() ; i++){
                stringBuilder.append(String.valueOf(person.getTreatments().get(i)) +";");
            }
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            stringBuilder.append("," + person.getNumberOfGivenTreatments()+",");
            stringBuilder.append(person.getDeleted());

            stringBuilder.append("\n");
            writer.append(stringBuilder.toString());
            stringBuilder.setLength(0);
            }
            stringBuilder.setLength(0);
            writer.flush();
        }
    }
    public Beautician getByID(int id) {
    	ArrayList<Beautician> beauticians = read();
    	for(Beautician beautician : beauticians) {
    		if(beautician.getId() == id) {
    			return beautician;
    		}
    	}
    	return null;
    }
    //create
    public boolean registerWorkers(String firstName, String lastName, String gender, String phoneNumber, String adress, String userName, String password, education levelOfEducation, double experience, double bonus, ArrayList<Integer> treatments, int numberOfGivenTreatments) throws IOException{
        ArrayList<Beautician> beauticians = read();
        double paycheck = Manager.setPaycheck(levelOfEducation, experience);
        Beautician beautician = new Beautician(Global.userID, firstName, lastName, gender, phoneNumber, adress, userName, password, levelOfEducation, experience, bonus, paycheck, treatments, numberOfGivenTreatments,false);
        beauticians.add(beautician);
        write(beauticians);
        Global.userID++;
        return true;
    }
    //update
    public boolean editBeautician(String oldUserName, String firstName, String lastName, String gender, String phoneNumber, String adress, String newUserName, String password, education levelOfEducation, double experience, double bonus, double paycheck, ArrayList<Integer> treatments,ArrayList<Beautician> beauticians) throws IOException{
    	for(Beautician person : beauticians){
            if(person.getUserName().equals(newUserName) && !oldUserName.equals(newUserName)){
                return false; //user name taken
            }
        }
        for(Beautician person : beauticians){
            if(person.getUserName().equals(oldUserName)){
                deleteBeautician(beauticians, person.getId());
                person.setFirstName(firstName);
                person.setLastName(lastName);
                person.setGender(gender);
                person.setAdress(adress);
                person.setGender(gender);
                person.setUserName(newUserName);
                person.setPassword(password);
                // CANNOT CHANGE NUMBER OF GIVEN TREATMENTS, AUTO
                person.setLevelOfEducation(levelOfEducation);
                person.setBonus(bonus);
                person.setExperience(experience);
                person.setPaycheck(paycheck);
                person.setTreatments(treatments);
                beauticians.add(person);
                write(beauticians);
                return true;
                }
            }
        return false;
    }
    //delete
    public void deleteBeautician(ArrayList<Beautician> beauticians, int id) throws IOException{
    	for(Beautician beautician : beauticians) {
    		if(beautician.getId() == id) {
    			beauticians.remove(beautician);
    			beautician.setDeleted(true);
    			beauticians.add(beautician);
    			write(beauticians);
    			break;
    		}
    	}
    }
    public ArrayList<Booked> cancelBookedNoShow(ClientManager cm, BookedManager bm, Booked booked, ArrayList<Booked> allBooked, Client client, ArrayList<Client> allClients) throws IOException {
    	allBooked.remove(booked);
    	allClients.remove(client);
    	booked.setStatusOfTreatment(status.NO_SHOW);
    	int money = (int) (client.getSpentMoney());
    	client.setSpentMoney(money);
    	allBooked.add(booked);
    	allClients.add(client);
    	bm.write(allBooked);
    	cm.write(allClients);
    	return allBooked;
    }
    public ArrayList<Booked> cancelBookedDone(ClientManager cm, BookedManager bm, Booked booked, ArrayList<Booked> allBooked, Client client, ArrayList<Client> allClients) throws IOException {
    	allBooked.remove(booked);
    	allClients.remove(client);
    	booked.setStatusOfTreatment(status.COMPLETED);
    	int money = (int) (client.getSpentMoney());
    	client.setSpentMoney(money);
    	allBooked.add(booked);
    	allClients.add(client);
    	bm.write(allBooked);
    	cm.write(allClients);
		return allBooked;
    }
    // bonus gledam da li je klijent u odredjenom periodu izvrsio minimalan broj tretmana za bonus
    public ArrayList<Beautician> setBonus(ArrayList<Booked> allBooked, BeauticianManager beauticianManager, ArrayList<Beautician> allBeauticians, BookedManager bookedManager, ManagerManager managerManager, int minimumTarget) throws IOException {
        for (Beautician beautician : allBeauticians) {
            int numberOfTreatments = 0;
            LocalDate date = LocalDate.now();
            LocalDate startDate = date.minusDays(30);
            for (Booked booked : allBooked) {
                if (booked.getUserNameOfBeautician().equals(beautician.getUserName())) {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                    LocalDate localDate = LocalDate.parse(booked.getDate(), formatter);
                    if (localDate.isAfter(startDate)) {
                        numberOfTreatments += 1;
                    }
                }
            }

            if (numberOfTreatments > minimumTarget) {
            	allBeauticians.remove(beautician);
                beautician.setBonus(2000);
                allBeauticians.add(beautician);
                beauticianManager.write(allBeauticians);
            }
        }

        return allBeauticians;
    }

}