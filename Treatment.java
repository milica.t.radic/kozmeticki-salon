package entity;
import java.util.ArrayList;

public class Treatment {

    private int id;
    private String nameOfService;
    private ArrayList<Integer> listOfTypesOfTreatments;

    public Treatment(int id, String nameOfService, ArrayList<Integer> listOfTypesOfTreatment){
        this.id = id;
        this.nameOfService = nameOfService;
        this.listOfTypesOfTreatments = listOfTypesOfTreatment;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNameOfService() {
        return nameOfService;
    }
    public void setNameOfService(String nameOfService) {
        this.nameOfService = nameOfService;
    }
    public ArrayList<Integer> getListOfTypesOfTreatments() {
        return listOfTypesOfTreatments;
    }
    public void setListOfTypesOfTreatments(ArrayList<Integer> listOfTypesOfTreatments) {
        this.listOfTypesOfTreatments = listOfTypesOfTreatments;
    }
    
}
