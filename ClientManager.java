package manage;
import entity.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

import konstante.*;
public class ClientManager{
    private static String clientsFile;
    private static ArrayList<Client> clients;

    public ClientManager(String clientsFile){
        this.clients = new ArrayList<Client>();
        this.clientsFile = clientsFile;
    }

    public Client getByID(int id) throws FileNotFoundException, IOException{
        ArrayList<Client> clients = read();
        for(Client client : clients) {
        	if(client.getId() == id) {
        		return client;
        	}
        }
        return null;
    }
    //read and write
    public ArrayList<Client> read() throws FileNotFoundException, IOException{
        ArrayList<Client> clients = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(clientsFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                if(values[0].equals(""))
                	break;
                Client client = new Client(Integer.parseInt(values[0]), values[1], values[2], values[3], values[4], values[5], values[6], values[7], Double.parseDouble(values[8]),Boolean.parseBoolean(values[9]),Boolean.valueOf(values[10]));
                clients.add(client);
                }

            }
        return clients;
        }

    public void write(ArrayList<Client> allUsers) throws IOException{
        try (FileWriter writer = new FileWriter(clientsFile, false)) {
            StringBuilder stringBuilder = new StringBuilder();
            for(Client person : allUsers){
            stringBuilder.append(person.getId() + ",");
            stringBuilder.append(person.getFirstName() + ",");
            stringBuilder.append(person.getLastName() + ",");
            stringBuilder.append(person.getGender() + ",");
            stringBuilder.append(person.getPhoneNumber() + ",");
            stringBuilder.append(person.getAdress() + ",");
            stringBuilder.append(person.getUserName() + ",");
            stringBuilder.append(person.getPassword() + ",");
            stringBuilder.append(person.getSpentMoney() + ",");
            stringBuilder.append(person.getLoyaltyCard() + ",");
            stringBuilder.append(person.getDeleted());
            stringBuilder.append("\n");
            writer.append(stringBuilder.toString());
            stringBuilder.setLength(0);
            }
            stringBuilder.setLength(0);
            writer.flush();
        }
    }
    //create
    public Client registerSelf(ArrayList<Client> allClients, String firstName, String lastName, String gender, String phoneNumber, String adress, String userName, String password, double spentMoney, boolean loyaltyCard) throws IOException{
    	for(Client c : allClients) {
        	if(c.getUserName().equals(userName) ) {
        		return null;
        	}
        }
    	if(!phoneNumber.replaceAll("\\D", "").matches("\\d+")) {
    		return null;
    		}
    	Client client = new Client(Global.userID, firstName, lastName, gender, phoneNumber, adress, userName, password, spentMoney, loyaltyCard,false);
        Global.userID++;
        clients.add(client);
        write(clients);
        return client;
    }
    //update
    public boolean editClient(ArrayList<Client> clients, String oldUserName, String firstName, String lastName, String gender, String phoneNumber, String adress, String newUserName, String password) throws IOException{
        for(Client person : clients){
            if(person.getUserName().equals(newUserName)){
                return false; //user name taken
            }
        }
        for(Client person : clients){
            if(person.getUserName().equals(oldUserName)){
                deleteClient(clients, person.getId());
                person.setFirstName(firstName);
                person.setLastName(lastName);
                person.setGender(gender);
                person.setAdress(adress);
                person.setGender(gender);
                person.setUserName(newUserName);
                person.setPassword(password);
                // CANNOT CHANGE HOW MUCH MONEY IS SPENT OR IF CLIENT HAS A CARD
                Client client = (Client) person;
                double spentMoney = client.getSpentMoney();
                boolean loyaltyCard = client.getLoyaltyCard();
                Client newClient = new Client(client.getId(),firstName,lastName,gender,phoneNumber,adress,newUserName,password,spentMoney,loyaltyCard,client.getDeleted());
                clients.remove(client);
                clients.add(newClient);
                write(clients);
                return true;

                }
            }
            return false;
        }

    public void deleteClient(ArrayList<Client> clients, int id) throws IOException{
        for(Client client : clients) {
        	if(client.getId() == id) {
        		clients.remove(client);
        		this.write(clients);
        		break;
        	}
        }
    }
    // opsta prijava za sve korisnike
    public Person logIn(String userName, String password, ArrayList<Client> allClients, ArrayList<Beautician> allBeauticians, ArrayList<Receptionist> allReceptionists, ArrayList<Manager> allManagers) {
    	for(Client client : allClients) {
    		if(client.getUserName().equals(userName) && client.getPassword().equals(password)) {
    			return client;
    		}
    	}
    	for(Manager manager : allManagers) {
    		if(manager.getUserName().equals(userName) && manager.getPassword().equals(password)) {
    			return manager;
    		}
    	}
    	for(Beautician beautician : allBeauticians) {
    		if(beautician.getUserName().equals(userName) && beautician.getPassword().equals(password)) {
    			return beautician;
    		}
    	}
    	for(Receptionist receptionist : allReceptionists) {
    		if(receptionist.getUserName().equals(userName) && receptionist.getPassword().equals(password)) {
    			return receptionist;
    		}
    	}
    	return null;
    }
    public ArrayList<Booked> cancelBooked(ClientManager cm, BookedManager bm, Booked booked, ArrayList<Booked> allBooked, Client client, ArrayList<Client> allClients) throws IOException {
    	allBooked.remove(booked);
    	allClients.remove(client);
    	booked.setStatusOfTreatment(status.CANCELED_BY_CLIENT);
    	double money = client.getSpentMoney() - 0.9*booked.getPrice();
    	client.setSpentMoney(money);
    	allBooked.add(booked);
    	allClients.add(client);
    	bm.write(allBooked);
    	cm.write(allClients);
    	return allBooked;
    }
}
