package entity;

public class ServiceTreatment {
    private int id;
    private String nameOfService;
    private String typeOfTheTreatment;
    private double price;
    private int time;

    public ServiceTreatment(int id, String nameOfService, String typeOfTheTreatment, int time, double price){
        this.id = id;
        this.nameOfService = nameOfService;
        this.typeOfTheTreatment = typeOfTheTreatment;
        this.time = time;
        this.price = price;
    }
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNameOfService() {
        return nameOfService;
    }
    public void setNameOfService(String nameOfService) {
        this.nameOfService = nameOfService;
    }
    public String getTypeOfTheTreatment() {
        return typeOfTheTreatment;
    }
    public void setTypeOfTheTreatment(String typeOfTheTreatment) {
        this.typeOfTheTreatment = typeOfTheTreatment;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
    
}
