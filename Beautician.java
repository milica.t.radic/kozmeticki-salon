package entity;
// import java.io.IOException;
// import java.time.LocalDate;
// import java.util.ArrayList;

import java.util.ArrayList;

import konstante.*;

public class Beautician extends Employe{
    private ArrayList<Integer> treatments;
    private int numberOfGivenTreatments;

    public Beautician(int id, String firstName, String lastName, String gender, String phoneNumber, String adress, String userName, String password, education levelOfEducation, double experience, double bonus, double paycheck, ArrayList<Integer> treatments, int numberOfGivenTreatments, boolean deleted) {
        super(id, firstName, lastName, gender, phoneNumber, adress, userName, password, levelOfEducation, experience, bonus, paycheck, deleted);
        this.treatments = treatments;
        this.numberOfGivenTreatments = numberOfGivenTreatments;
    }

    public ArrayList<Integer> getTreatments() {
        return treatments;
    }
    public void setTreatments(ArrayList<Integer> treatments) {
        this.treatments = treatments;
    }
    public int getNumberOfGivenTreatments() {
        return numberOfGivenTreatments;
    }
    public void setNumberOfGivenTreatments(int numberOfGivenTreatments) {
        this.numberOfGivenTreatments = numberOfGivenTreatments;
    }

}