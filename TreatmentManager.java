package manage;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import entity.Global;
import entity.ServiceTreatment;
import entity.Treatment;

public class TreatmentManager {

    private static String serviceFile;
    private ArrayList<Treatment> allServices;

    public TreatmentManager(String serviceFile){
        this.allServices = new ArrayList<Treatment>();
        this.serviceFile = serviceFile;
    }
    
        // READ FROM THE FILE
        public ArrayList<Treatment> read() throws FileNotFoundException, IOException {
            ArrayList<Treatment> allServices = new ArrayList<>();
            try (BufferedReader br = new BufferedReader(new FileReader(serviceFile))) {
                String line;
                ArrayList<Integer> listOfTreatments = new ArrayList<Integer>();
                while ((line = br.readLine()) != null) {
                    String[] values = line.split(",");
                    if(values[0].equals(""))
                    	break;
                    if (values.length >= 3) {
                        String[] types = values[2].split(";");
                        for (String type : types) {
                            listOfTreatments.add(Integer.parseInt(type));
                        }
                    }
                    Treatment treatment = new Treatment(Integer.parseInt(values[0]),values[1],listOfTreatments);
                    allServices.add(treatment);
                    listOfTreatments = new ArrayList<Integer>();
                }
            }
            return allServices;
        }
        // WRITE IN THE FILE
        public void write(ArrayList<Treatment> allServices) throws IOException{
            try (FileWriter writer = new FileWriter(serviceFile, false)) {
                StringBuilder stringBuilder = new StringBuilder();
                for(Treatment service : allServices){
                stringBuilder.append(service.getId() + ",");
                stringBuilder.append(service.getNameOfService() + ",");
                ArrayList<Integer> listOfTypeOfTheTreatments = service.getListOfTypesOfTreatments();
                for(int i = 0; i < listOfTypeOfTheTreatments.size();  i++){
                    stringBuilder.append(String.valueOf(listOfTypeOfTheTreatments.get(i)) + ";");
                }
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                stringBuilder.append("\n");
                writer.append(stringBuilder.toString());
                stringBuilder.setLength(0);
                }
            }
    }
        // CREATE JUST THE SERVICE
        public Treatment createTreatment(String nameOfService, ArrayList<Integer> listOfTypesOfTreatment) throws IOException{
        	ArrayList<Treatment> allTreatments = read();
        	Treatment treatment = new Treatment(Global.serviceID, nameOfService, listOfTypesOfTreatment);
            Global.serviceID++;
            allTreatments.add(treatment);
            write(allTreatments);
            return treatment;
        }
        // DELETE THE SERVICE
        public void deleteTreatmentByID(ServiceTreatmentManager serviceTreatmentManager, ArrayList<ServiceTreatment> allServiceTreatments, ArrayList<Treatment> allTreatments, int id) throws IOException{
            for(Treatment treatment : allTreatments) {
            	if(treatment.getId() == id) {
            		allTreatments.remove(treatment);
            		this.write(allTreatments);
                    for(ServiceTreatment service : allServiceTreatments) {
                    	if(service.getNameOfService().equals(treatment.getNameOfService())) {
                    		allServiceTreatments.remove(service);
                    		serviceTreatmentManager.write(allServiceTreatments);
                    		break;
                    	}
                    }
                   break;
            	}
            }
        }
        // UPDATE THE SERVICE (promenom tretmana)
        public void updateTreatment(Treatment treatment, ArrayList<Integer> listOfTypesOfTreatment) throws IOException{
            //nema bas smisla menjati ime tretmana
        	ArrayList<Treatment> allTreatments = read();
        	allTreatments.remove(treatment);
            treatment.setListOfTypesOfTreatments(listOfTypesOfTreatment);
            //ostaviti postojece tretmane da se izvrse
            allTreatments.add(treatment);
            write(allTreatments);
        }
        public Treatment getByID(int id) throws FileNotFoundException, IOException {
        	ArrayList<Treatment> allTreatments = read();
        	for(Treatment treatment : allTreatments) {
        		if(treatment.getId() == id) {
        			return treatment;
        		}
        	}
        	return null;
        }
}
