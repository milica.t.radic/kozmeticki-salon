package manage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import entity.Beautician;
import entity.Booked;
import entity.Client;
import entity.Global;
import entity.Manager;
import entity.ServiceTreatment;
import konstante.status;

public class BookedManager {
    private static String bookedFile;
    private static ArrayList<Booked> allBooked;

    public BookedManager(String bookedFile){
        this.allBooked = new ArrayList<Booked>();
        this.bookedFile = bookedFile;
    }

    // read and write
    public ArrayList<Booked> read() {
        ArrayList<Booked> allBooked = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(bookedFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                if(values[0].equals(""))
                	break;
                Booked treatment = new Booked(Integer.parseInt(values[0]), values[1], values[2], Double.parseDouble(values[3]), values[4], Integer.parseInt(values[5]), values[6], Enum.valueOf(status.class, values[7]),values[8],values[9]);
                allBooked.add(treatment);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allBooked;
    }
    public void write(ArrayList<Booked> allBooked) throws IOException{
        try (FileWriter writer = new FileWriter(bookedFile, false)) {
            for(Booked booked : allBooked){
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(booked.getBookedID() + ",");
            stringBuilder.append(booked.getUserNameOfClient() + ",");
            stringBuilder.append(booked.getUserNameOfBeautician() + ",");
            stringBuilder.append(booked.getPrice() + ",");
            stringBuilder.append(booked.getNameOfTreatment() + ",");
            stringBuilder.append(String.valueOf(booked.getId())+",");
            stringBuilder.append(booked.getTypeOfTreatment() + ",");
            String status = String.valueOf(booked.getStatusOfTreatment());
            stringBuilder.append(status + ",");
            String dateStr = booked.getDate();
            DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("d/M/yyyy");
            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate date = LocalDate.parse(dateStr, inputFormatter);
            String formattedDate = date.format(outputFormatter);
            stringBuilder.append(formattedDate + ",");
            stringBuilder.append(booked.getTime());
            stringBuilder.append("\n");
            writer.append(stringBuilder.toString());
            stringBuilder.setLength(0);
            }
        }
    }
    public Booked getByID(int id) {
    	ArrayList<Booked> allBooked = read();
    	for(Booked booked : allBooked) {
    		if(booked.getBookedID() == id) {
    			return booked;
    		}
    	}
    	return null;
    }
    // create
    public boolean bookAppointment(ClientManager clientManager, ArrayList<Booked> allBooked, ArrayList<Client> clients, Client client, ServiceTreatment typeOfTreatment, Beautician beautician, String date, String time) throws IOException{
        if(isBooked(date,time,beautician.getUserName(),allBooked)) {
        	return false;
        }
        int numerator = typeOfTreatment.getTime();
        int denominator = 60;
        double result = Math.ceil((double) numerator / denominator);
        int roundedResult = (int) result;
    	Booked booked = new Booked(Global.bookedID, client.getUserName(), beautician.getUserName(),Manager.tenPercentOff(typeOfTreatment, client), typeOfTreatment.getTypeOfTheTreatment(),typeOfTreatment.getId(),typeOfTreatment.getNameOfService(),status.BOOKED,date,time);
        result -= 1;
        while(result >= 1) {
        	time = String.valueOf(Integer.parseInt(time) + 1);
        	result -= 1;
        	Booked b = new Booked(0, "",beautician.getUserName(),typeOfTreatment.getPrice(), typeOfTreatment.getTypeOfTheTreatment(),typeOfTreatment.getId(),typeOfTreatment.getNameOfService(),status.BOOKED,date,time);
        	allBooked.add(b);
        	time = String.valueOf(time);
        }
    	allBooked.add(booked);
        Global.bookedID++;
        this.write(allBooked);
        clientManager.deleteClient(clients, client.getId());
        client.setSpentMoney(client.getSpentMoney() + Manager.tenPercentOff(typeOfTreatment, client));
        clients.add(client);
        clientManager.write(clients);
        return true;
    }
    public boolean isBooked(String date, String time, String beautician, ArrayList<Booked> allBooked){
        for(Booked booked : allBooked) {
        	if(booked.getUserNameOfBeautician().equals(beautician) && booked.getDate().equals(date) && booked.getTime().equals(time)) {
        		return true;
        	}
        }
        return false;
    }
    // update
    public boolean updateBookedAppointment(BeauticianManager beauticianManager, int id, String name,  ArrayList<Booked> allBooked, Booked booked, String userNameOfClient, String userNameOfBeautician, double price, String date, String time) throws IOException{
        if(!isBooked(date,time,userNameOfBeautician,allBooked)){
            boolean found = false;
            deleteBookedAppointment(booked.getBookedID());
            allBooked = this.read();
            booked.setId(id);
            booked.setUserNameOfBeautician(userNameOfBeautician);
            booked.setDate(date);
            booked.setNameOfTreatment(name);
            booked.setPrice(price);
            booked.setUserNameOfClient(userNameOfClient);
            //provera da li kozmeticar obavlja tu uslugut
            booked.setTime(time);
            //provera vremena i dostupnosti
/*
            ArrayList<Beautician> beauticians = beauticianManager.read();
            for(Beautician beautician :beauticians){
                if(beautician.getUserName().equals(userNameOfBeautician)){
                    for(int i = 0 ; i < beautician.getTreatments().size() ; i++){
                        if(beautician.getTreatments().contains(userNameOfBeautician)){
                            found = true;
                            break;
                        }
                    }
                }
            }
*/
            allBooked.add(booked);
            write(allBooked);
            return true;
        }
        return false;

    }
    // DELETE BOOKED
    public void deleteBookedAppointment(int id) throws IOException{
        ArrayList<Booked> allBooked = this.read();
    	for(Booked booked : allBooked) {
    		if(booked.getBookedID()==id) {
    			allBooked.remove(booked);
    			this.write(allBooked);
    			break;
    		}
    	}
    }
}
