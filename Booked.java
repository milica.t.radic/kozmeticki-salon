package entity;
import konstante.*;

public class Booked {
    private int bookedID;
    private String userNameOfClient, userNameOfBeautician, time, date, typeOfTreatment, nameOfTreatment;
    private double price;
    private int id;
    private status statusOfTreatment;

    public Booked(int bookedID, String userNameOfClient, String userNameOfBeautician, double price, String nameOfTreatment, int id, String typeOfTreatment, status statusOfTreatment, String date, String time){
        this.bookedID = bookedID;
        this.userNameOfClient = userNameOfClient;
        this.userNameOfBeautician = userNameOfBeautician;
        this.price = price;
        this.nameOfTreatment = nameOfTreatment;
        this.typeOfTreatment = typeOfTreatment;
        this.id = id;
        this.statusOfTreatment = statusOfTreatment;
        this.date= date;
        this.time = time;
    }

    public int getBookedID() {
        return bookedID;
    }
    public void setBookedID(int bookedID) {
        this.bookedID = bookedID;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getUserNameOfClient() {
        return userNameOfClient;
    }
    public void setUserNameOfClient(String userNameOfClient) {
        this.userNameOfClient = userNameOfClient;
    }
    public String getUserNameOfBeautician() {
        return userNameOfBeautician;
    }
    public void setUserNameOfBeautician(String userNameOfBeautician) {
        this.userNameOfBeautician = userNameOfBeautician;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public String getNameOfTreatment() {
        return nameOfTreatment;
    }
    public void setNameOfTreatment(String nameOfTreatment) {
        this.nameOfTreatment = nameOfTreatment;
    }
    public String getTypeOfTreatment() {
        return typeOfTreatment;
    }
    public void setTypeOfTreatment(String typeOfTreatment) {
        this.typeOfTreatment = typeOfTreatment;
    }
    public status getStatusOfTreatment() {
        return statusOfTreatment;
    }
    public void setStatusOfTreatment(status statusOfTreatment) {
        this.statusOfTreatment = statusOfTreatment;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }
}

