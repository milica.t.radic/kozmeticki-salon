package manage;

import utils.AppSettings;

public class ManagerFactory {
	private AppSettings appSettings;
	private ClientManager clientMng;
    private TreatmentManager serviceMng;
    private BeauticianManager beauticianMng;
    private ReceptionistManager receptionistMng;
    private ManagerManager managerMng;
    private BookedManager bookedMng;
    private ServiceTreatmentManager treatmentMng;
	public ClientManager getClientMng() {
        return clientMng;
    }
    public BeauticianManager getBeauticianMng() {
        return beauticianMng;
    }
	public ReceptionistManager getReceptionistMng() {
        return receptionistMng;
    }
    public ManagerManager getManagerMng() {
        return managerMng;
    }
	public BookedManager getBookedMng() {
        return bookedMng;
    }
    public ServiceTreatmentManager getTreatmentMng() {
        return treatmentMng;
    }
	
	public TreatmentManager getServiceMng() {
        return serviceMng;
    }
    public ManagerFactory(AppSettings appSettings) {
		this.appSettings = appSettings;
		this.managerMng = new ManagerManager(this.appSettings.getManagerFile());
		this.beauticianMng = new BeauticianManager(this.appSettings.getBeauticianFile());
        this.bookedMng = new BookedManager(this.appSettings.getBookedFile());
        this.clientMng = new ClientManager(this.appSettings.getClientsFile());
        this.receptionistMng = new ReceptionistManager(this.appSettings.getReceptionistFile());
        this.serviceMng = new TreatmentManager(this.appSettings.getServiceFile());
        this.treatmentMng = new ServiceTreatmentManager(this.appSettings.getTreatmentFile());
	}
	
}

